/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epsi.mtp.projet1;

import java.awt.Color;

/**
 *
 * @author lothar
 */
public class Chien extends Animal {

    public Chien(Color couleur, String sexe) {
        super(couleur, sexe);
    }
    
    public void seDeplacer() {
        this.marcher();
    }
    
    private void marcher() {
        System.out.println("Je marche");
    }

    @Override
    public String toString() {
        return "Chien{" + super.toString()+ '}';
    }  
}
