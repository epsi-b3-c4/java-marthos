/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epsi.mtp.projet1;

import java.awt.Color;

/**
 *
 * @author lothar
 */
public class Oiseau extends Animal{
    
    private void voler() {
        System.out.println("I fly");
    }
    
    @Override
    public String toString() {
        return "Oiseau{" + super.toString()+ '}';
    }

    @Override
    public void seDeplacer() {
        this.voler();
    }
}
