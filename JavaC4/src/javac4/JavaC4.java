/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javac4;

import epsi.mtp.projet1.Personne;

/**
 *
 * @author Julien Raynal
 */
public class JavaC4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Personne p;
        p = new Personne();
        p.setNom("Raynal");
        p.setPrenom("Julien");
        System.out.println(p.getNom()+ " " +p.getPrenom());
    }   
}
